/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.samples.binder.simple.model;

import javax.validation.constraints.Max;
import lombok.*;

/**
 * Person model.
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class PersonComplex {

    private String firstName;

    private String lastName;

    private String phoneNumber;

    @Max(value = 100, message = "Age cannot greater than 100 years.")
    private Integer age;

    private Address address;

    public String getFullName() {
        StringBuilder sb = new StringBuilder();
        if (firstName != null) {
            sb.append(firstName);
        }
        if (lastName != null) {
            sb.append(" ").append(lastName);
        }
        return sb.toString();
    }
}
