/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.samples.binder.simple.model;

import java.util.Date;
import lombok.*;

/**
 * Person model.
 *
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Person {

    private String firstName;

    private String lastName;

    private Date dob;

    public String getFullName() {
        StringBuilder sb = new StringBuilder();
        if (firstName != null) {
            sb.append(firstName);
        }
        if (lastName != null) {
            sb.append(" ").append(lastName);
        }
        return sb.toString();
    }

}
