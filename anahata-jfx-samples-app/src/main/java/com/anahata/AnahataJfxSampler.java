/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata;

import fxsampler.FXSamplerProject;
import fxsampler.model.WelcomePage;
import javafx.scene.control.Label;
/**
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class AnahataJfxSampler implements FXSamplerProject {

    @Override
    public String getProjectName() {
        return "Anahata-JFX";
    }

    @Override
    public String getSampleBasePackage() {
        return "com.anahata.samples";
    }

    
    
    @Override
    public WelcomePage getWelcomePage() {
        WelcomePage welcomePage = buildWelcomePage();
        return welcomePage;
    }
    
    private WelcomePage buildWelcomePage() {
        WelcomePage welcomePage = new WelcomePage("Welcome to Anahata-jfx", new Label("Anahata"));
        return welcomePage;
    }
    
}
