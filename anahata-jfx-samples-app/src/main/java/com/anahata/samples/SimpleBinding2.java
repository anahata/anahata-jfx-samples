/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.samples;

import com.anahata.AnahataSampleBase;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class SimpleBinding2 extends AnahataSampleBase {

    @Override
    public Node getPanel(Stage stage) {
        return new Label("Test");
//        PersonController pc = Cdi.get(PersonController.class);
//        return pc.getRootPane();
    }

    public static void main(String[] args) {
        launch(args);
    }
  
}
