package com.anahata.samples.abinder.simple;

import com.anahata.jfx.bind.*;
import com.anahata.samples.binder.simple.model.Person;
import com.anahata.samples.binder.simple.model.Person_mm;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import lombok.Getter;

/**
 * 
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class SimplePersonController {

    @Inject
    private Binder binder;

    // == controls ==
    @BindView
    @Getter
    private StackPane rootPane;

    @BindSubView
    private GridPane gridPane;

    private Label firstNameLabel;

    @Bind(property = Person_mm.firstName.class)
    private TextField firstName;

    private Label lastNameLabel;

    @Bind(property = Person_mm.lastName.class)
    private TextField lastName;

    @Bind(property = Person_mm.fullName.class)
    private Label fullNameLabel;

    // == model ==    
    @BindModel
    private final ObjectProperty<Person> model = new SimpleObjectProperty<>(new Person());

    // == init ==
    @PostConstruct
    public void postConstruct() {        

        gridPane = new GridPane();
        gridPane.setHgap(8);
        gridPane.setVgap(8);
        gridPane.setPadding(new Insets(50, 0, 0, 0));
        ColumnConstraints col1 = new ColumnConstraints(100);
        ColumnConstraints col2 = new ColumnConstraints(200, 200, Double.MAX_VALUE);
        gridPane.getColumnConstraints().addAll(col1, col2);

        firstNameLabel = new Label("First Name");
        gridPane.add(firstNameLabel, 0, 0);

        firstName = new TextField();
        gridPane.add(firstName, 1, 0);

        lastNameLabel = new Label("Last Name");
        gridPane.add(lastNameLabel, 0, 1);

        lastName = new TextField();
        gridPane.add(lastName, 1, 1);

        gridPane.add(new Label("Full Name"), 0, 2);

        fullNameLabel = new Label();
        gridPane.add(fullNameLabel, 1, 2);

        rootPane = new StackPane(gridPane);

        binder.init(this);
    }
}
