/*
 * Copyright 2014 Anahata.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anahata.samples.abinder.complex;

import com.anahata.jfx.bind.BindModel;
import com.anahata.jfx.bind.BindingController;
import com.anahata.samples.binder.simple.model.Person;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
/**
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class SubControllerSampleMainController extends BindingController {
    
    @BindModel
    private final ObjectProperty<Person> person = new SimpleObjectProperty<>();
}
