package com.anahata.samples.abinder.simple;

import com.anahata.jfx.bind.*;
import com.anahata.jfx.bind.converter.string.LocalDateTimeDateConverter;
import com.anahata.jfx.fxml.FXMLController;
import com.anahata.jfx.scene.control.AutoCompleteTextField;
import com.anahata.samples.binder.simple.model.Person;
import com.anahata.samples.binder.simple.model.Person_mm;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import lombok.extern.slf4j.Slf4j;

/**
 * Here we will extend BindingController, which has common code to
 * all controllers and adds Binder functionality.
 *
 */
@Slf4j
@FXMLController("/samples/abinder/SimpleBindingFXML.fxml")
public class SimplePersonControllerFXML extends BindingController {

    @FXML
    @Bind(property = Person_mm.firstName.class)
    private AutoCompleteTextField<Person> firstName;

    @FXML
    @Bind(property = Person_mm.lastName.class)
    private TextField lastName;

    @FXML
    @Bind(property = Person_mm.fullName.class)
    private Label fullNameLabel;

    @FXML
    @Bind(property = Person_mm.dob.class, converter = LocalDateTimeDateConverter.class)
    private jfxtras.scene.control.LocalDateTimeTextField dateTime;
    // == model ==
    @BindModel
    private final ObjectProperty<Person> model = new SimpleObjectProperty<>(new Person());

    @Override
    protected void postInit() {
        List<Person> list = Arrays.asList(
                new Person("Muhammad", "Zeshan", new Date()),
                new Person("Goran", "Lochert", new Date()),
                new Person("Pablo", "Pina", new Date()));

        PersonAutoComplete.init(firstName, list);
    }

    @FXML
    void find(ActionEvent actionEvent) {
        Person person = (Person)actionEvent.getSource();
        model.set(person);
        getBinder().bindFromModel();
    }

}
