/*
 * Copyright 2014 Anahata Technologies Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anahata.samples.abinder.simple;

import com.anahata.jfx.bind.Bind;
import com.anahata.jfx.bind.BindingController;
import com.anahata.jfx.fxml.FXMLController;
import com.anahata.samples.binder.simple.model.Elephant_mm;
import com.anahata.samples.binder.simple.model.PetType;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import lombok.extern.slf4j.Slf4j;

/**
 * FXML Controller class
 *
 * @author Arslan
 */
@Slf4j
@FXMLController("/samples/abinder/Elephant.fxml")
public class ElephantController extends BindingController {

    @FXML
    @Bind(property = Elephant_mm.height.class)
    private TextField height;

    @FXML
    @Bind(property = Elephant_mm.run.class)
    private CheckBox run;

    @FXML
    @Bind(property = Elephant_mm.vegitarian.class)
    private CheckBox vegitarian;

    @FXML
    @Bind(property = Elephant_mm.nickName.class)
    private TextField nickName;

    @FXML
    @Bind(property = Elephant_mm.type.class)
    private ComboBox<PetType> type;

    @FXML
    @Bind(property = Elephant_mm.color.class)
    private TextField color;

    @FXML
    @Bind(property = Elephant_mm.age.class)
    private TextField age;

    @FXML
    @Bind(property = Elephant_mm.weight.class)
    private TextField weight;

}
