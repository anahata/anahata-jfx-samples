/*
 * Copyright 2014 Anahata Technologies Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anahata.samples.abinder.simple;

import com.anahata.jfx.bind.Bind;
import com.anahata.jfx.bind.BindModel;
import com.anahata.jfx.bind.BindView;
import com.anahata.jfx.bind.BindingController;
import com.anahata.jfx.bind.filter.PositiveIntegerKeystrokeFilter;
import com.anahata.jfx.fxml.FXMLController;
import com.anahata.samples.binder.simple.model.PersonComplex;
import com.anahata.samples.binder.simple.model.PersonComplex_mm;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import lombok.extern.slf4j.Slf4j;

/**
 * FXML Controller class
 *
 * @author Arslan
 */
@Slf4j
@FXMLController("/samples/abinder/SampleKeyStrokeFilter.fxml")
public class SamplePersonKeyStrokeFilterController extends BindingController {

    @BindView
    @FXML
    private StackPane rootPane;

    @FXML
    @Bind(property = PersonComplex_mm.firstName.class)
    private TextField firstName;

    @FXML
    @Bind(property = PersonComplex_mm.lastName.class)
    private TextField lastName;

    @FXML
    @Bind(property = PersonComplex_mm.age.class, keystrokeFilter = PositiveIntegerKeystrokeFilter.class)
    private TextField age;

    @FXML
    @Bind(property = PersonComplex_mm.fullName.class)
    private Label fullName;

    @FXML
    @Bind(property = PersonComplex_mm.age.class)
    private Label ageLabel;

    @BindModel
    private final ObjectProperty<PersonComplex> model = new SimpleObjectProperty<>(new PersonComplex());

}
