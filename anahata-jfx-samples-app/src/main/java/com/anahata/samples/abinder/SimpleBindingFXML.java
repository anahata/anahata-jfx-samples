/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.samples.abinder;

import com.anahata.AnahataSampleBase;
import com.anahata.jfx.JfxUtils;
import com.anahata.samples.abinder.simple.SimplePersonControllerFXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.stage.Stage;
import static javafx.application.Application.*;

/**
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class SimpleBindingFXML extends AnahataSampleBase {

    private final Node node;

    private final SimplePersonControllerFXML controller;

    public SimpleBindingFXML() {
        System.out.println("Using load with class param");
        FXMLLoader loader = JfxUtils.load(SimplePersonControllerFXML.class);
        node = loader.getRoot();
        controller = loader.getController();
    }

    @Override
    public Node getPanel(Stage stage) {
        super.getPanel(stage); // to add stylesheets        
        return node;
    }

    @Override
    public String getSampleDescription() {
        return "Demonstration of simple binding with FXML controller.";
    }

    public static void main(String[] args) {
        launch(args);
    }
}
