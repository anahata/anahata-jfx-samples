/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.samples.abinder.simple;

import com.anahata.jfx.scene.control.AutoCompleteTextField;
import com.anahata.jfx.scene.control.AutoCompleteTextField.DataFormatter;
import com.anahata.jfx.scene.control.AutoCompleteTextField.Mode;
import com.anahata.samples.binder.simple.model.Person;
import java.util.List;
import javafx.collections.FXCollections;

/**
 *
 * @author Muhammad Zeshan <zeshan@anahata-it.com.au>
 */
public class PersonAutoComplete {

    public static void init(AutoCompleteTextField<Person> atf, List<Person> list) {
        atf.setMode(Mode.MIXED);
        atf.setItems(FXCollections.observableList(list));
        atf.setDataFormatter(new DataFormatter<Person>() {

            @Override
            public String toListViewString(Person item) {
                return item.getFullName();
            }

            @Override
            public String toTextFieldString(Person item) {
                return item.getFirstName();
            }
        });

    }

}
