package com.anahata.samples.abinder.simple;

import com.anahata.jfx.bind.*;
import com.anahata.samples.binder.simple.model.Person;
import com.anahata.samples.binder.simple.model.Person_mm;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import lombok.Getter;

/**
 * 
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class PersonController {

    @Inject
    private Binder binder;

    @BindView
    @Getter
    private StackPane rootPane;

    private VBox vbox;

    //== first tab ==============================================
    @BindSubView
    private VBox firstVbox;

    private GridPane firstGridPane;

    private Label firstNameLabel;

    @Bind(property = Person_mm.firstName.class)
    private TextField firstName;

    private Label lastNameLabel;

    @Bind(property = Person_mm.lastName.class)
    private TextField lastName;

//    @Bind(property = Person_mm.age.class)
//    private TextField age;

    //== other ==================================================
    private Button validate;

    @BindModel
    private final ObjectProperty<Person> model = new SimpleObjectProperty<>(new Person());

    @PostConstruct
    public void postConstruct() {
        firstVbox = new VBox(8);
        firstVbox.setPadding(new Insets(8, 8, 8, 100));

        firstGridPane = new GridPane();
        firstGridPane.setHgap(8);
        firstGridPane.setVgap(8);
        firstGridPane.setPadding(new Insets(0, 0, 0, 50));
        ColumnConstraints col1 = new ColumnConstraints(100);
        ColumnConstraints col2 = new ColumnConstraints(100, 100, Double.MAX_VALUE);
        firstGridPane.getColumnConstraints().addAll(col1, col2);

        firstNameLabel = new Label("First Name");
        firstGridPane.add(firstNameLabel, 0, 0);

        firstName = new TextField();
        firstGridPane.add(firstName, 1, 0);

        lastNameLabel = new Label("Last Name");
        firstGridPane.add(lastNameLabel, 0, 1);

        lastName = new TextField();
        firstGridPane.add(lastName, 1, 1);

        firstGridPane.add(new Label("Age"), 0, 2);

//        age = new TextField();
//        firstGridPane.add(age, 1, 2);

        Button reset = new Button("Reset Age to null");
        firstGridPane.add(reset, 0, 3);

        reset.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
//                model.get().setAge(null);
                binder.bindFromModel();
            }
        });

        Button setTo22 = new Button("Set Age to 22");
        firstGridPane.add(setTo22, 1, 3);

        setTo22.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
//                model.get().setAge(22);
                binder.bindFromModel();
            }
        });

        firstVbox.getChildren().add(firstGridPane);

        vbox = new VBox(8);

        validate = new Button("Validate");

        validate.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                binder.validate(true);
            }
        });
       

        vbox.getChildren().add(firstVbox);
        vbox.getChildren().add(validate);


        rootPane = new StackPane(vbox);

        binder.init(this);
    }
}
