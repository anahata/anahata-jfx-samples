/*
 * Copyright 2014 Anahata.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anahata.samples.abinder;

import com.anahata.AnahataSampleBase;
import com.anahata.jfx.JfxUtils;
import com.anahata.samples.abinder.complex.SubControllerSampleMainController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.stage.Stage;
/**
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class SampleWithSubcontrollerMain extends AnahataSampleBase {
        private final Node node;

    private final SubControllerSampleMainController controller;

    public SampleWithSubcontrollerMain() {
        FXMLLoader loader = JfxUtils.load(SubControllerSampleMainController.class);
        node = loader.getRoot();
        controller = loader.getController();
    }

    @Override
    public Node getPanel(Stage stage) {
        super.getPanel(stage); // to add stylesheets        
        return node;
    }

    @Override
    public String getSampleDescription() {
        return "Demonstration of binding with subcontroller";
    }

    public static void main(String[] args) {
        launch(args);
    }
}
