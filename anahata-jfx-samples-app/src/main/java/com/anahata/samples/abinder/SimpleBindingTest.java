/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.samples.abinder;

import com.anahata.AnahataSampleBase;
import com.anahata.samples.abinder.simple.PersonController;
import com.anahata.util.cdi.Cdi;
import javafx.scene.Node;
import javafx.stage.Stage;

/**
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class SimpleBindingTest extends AnahataSampleBase {

    // get controller from CDI
    private final PersonController controller = Cdi.get(PersonController.class);

    @Override
    public Node getPanel(Stage stage) {
        super.getPanel(stage); // to add stylesheets        
        return controller.getRootPane();
    }

    @Override
    public String getSampleDescription() {
        return "Demonstration of simple binding.";
    }

    
    
    public static void main(String[] args) {
        launch(args);
    }
    
    

}
