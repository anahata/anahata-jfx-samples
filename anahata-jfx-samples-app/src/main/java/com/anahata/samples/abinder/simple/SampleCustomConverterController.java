/*
 * Copyright 2014 Anahata Technologies Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.anahata.samples.abinder.simple;

import com.anahata.jfx.bind.Bind;
import com.anahata.jfx.bind.BindModel;
import com.anahata.jfx.bind.BindingController;
import com.anahata.jfx.bind.converter.DateLocalDateConverter;
import com.anahata.jfx.bind.converter.string.ToStringConverter;
import com.anahata.jfx.fxml.FXMLController;
import com.anahata.samples.binder.simple.model.Employee;
import com.anahata.samples.binder.simple.model.Employee_mm;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Arslan
 */
@Slf4j
@FXMLController("/samples/abinder/SampleCustomConverter.fxml")
public class SampleCustomConverterController extends BindingController {

    @FXML
    @Bind(property = Employee_mm.joiningDate.class, converter = DateLocalDateConverter.class)
    private DatePicker dp;

    @FXML
    @Bind(property = Employee_mm.joiningDate.class, converter = ToStringConverter.class)
    private Label dateLabel;

    @BindModel
    private final ObjectProperty<Employee> model = new SimpleObjectProperty<>(new Employee());

}
