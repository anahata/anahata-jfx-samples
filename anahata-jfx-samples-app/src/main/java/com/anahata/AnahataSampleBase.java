/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata;

import fxsampler.SampleBase;
import javafx.scene.Node;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
@Slf4j
public abstract class AnahataSampleBase extends SampleBase {

    @Override
    public String getProjectVersion() {
        return "1.5.2-SNAPSHOT";
    }
        
    @Override
    public Node getPanel(Stage stage) {
        // TODO: fix this so it is better without returning null
        String css = getControlStylesheetURL();
        if (css != null && stage.getScene() != null) {            
            stage.getScene().getStylesheets().add(css);
        }
        return null;
    }

    @Override
    public String getSampleName() {
        return this.getClass().getSimpleName();
    }

    @Override
    public String getJavaDocURL() {
        return "";
    }

    @Override
    public String getControlStylesheetURL() {
        return "/css/SampleApp.css";
    }

    @Override
    public String getProjectName() {
        return "Anahata-JFX";
    }

    @Override
    public String getSampleSourceURL() {
        return null;
    }

}
