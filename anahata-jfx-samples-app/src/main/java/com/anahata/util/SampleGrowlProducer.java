package com.anahata.util;

import com.anahata.jfx.message.GrowlParent;
import javafx.scene.layout.Pane;
import javax.enterprise.inject.Produces;
import lombok.Getter;
import lombok.Setter;

/**
 * Growl producer for samples.
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class SampleGrowlProducer {

    @Getter
    @Setter
    private static Pane pane;

    @Produces
    @GrowlParent
    public Pane getGrowlParent() {
        return pane;
    }
}
