package com.anahata.util;

import javax.enterprise.inject.Produces;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * Validator producer for samples.
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class SampleValidatorProducer {

    @Produces
    public Validator getValidator() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        return factory.getValidator();
    }
}
