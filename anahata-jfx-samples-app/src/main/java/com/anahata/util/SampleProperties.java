package com.anahata.util;

import com.anahata.util.config.internal.ApplicationPropertiesFactory;
import java.util.Properties;
import javax.annotation.PostConstruct;
import lombok.Getter;

/**
 * Configure properties for Samples.
 *
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
public class SampleProperties implements ApplicationPropertiesFactory {

    @Getter
    private static final Properties props = new Properties();

    @Override
    public Properties getAppProperties() {
        return props;
    }

    @PostConstruct
    public void init() {
        getProps().setProperty("anahatajfx.message.growl.fadein.millis", "100");
        getProps().setProperty("anahatajfx.message.growl.fadeout.millis", "100");
        getProps().setProperty("anahatajfx.message.growl.pause.millis", "100");
        getProps().setProperty("anahatajfx.icon.delete", "delete.png");
        getProps().setProperty("anahatajfx.css.readonly", "readonly.css");
    }
}
