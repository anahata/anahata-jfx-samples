package com.anahata.util;

import com.anahata.jfx.config.JavaFXDynamicConfig;
import javax.enterprise.context.ApplicationScoped;

/**
 * 
 * @author Goran Lochert <goran@anahata-it.com.au>
 */
@ApplicationScoped
public class SampleJavaFXDynamicConfig implements JavaFXDynamicConfig {
    @Override
    public String getCssPath() {
        return "/";
    }
}
